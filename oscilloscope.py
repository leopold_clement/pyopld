import typing

class BaseOscilloscope():
    def __init__(self) -> None:
        pass

    @property
    def nb_channel(self) -> int :
        """Nombre de channel de l'oscilloscope"""
        raise NotImplementedError

    def get_chanels(self) -> typing.List[float]:
        """Récupère toutes les données du cache de l'oscilloscope"""
        raise NotImplementedError

    def set_time_scale(self, sec_per_div : float) -> None :
        """Setter de l'echelle temporelle

        Args:
            sec_per_div (float): Nombre de seconde par division de l'écran

        Raises:
            NotImplementedError: Pas d'implémentation réel
        """
        raise NotImplementedError

    def set_tension_scale(self, volt_per_div : float, channel : int) -> None :
        """Setter de l'echelle vertical

        Args:
            volt_per_div (float): Nombre de volt par division de l'écran
            channel (int): Numéro de la voie à modifier

        Raises:
            NotImplementedError: Pas d'implementation réel
        """
        raise NotImplementedError

